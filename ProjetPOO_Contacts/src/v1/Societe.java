package v1;/*
TODO:
    • Constructeur spécialisé
    • Accès en lecture aux divers attributs (inspecteurs)
    • Affichage d'une société (toString)
    • Egalité de deux sociétés basée sur la comparaison des noms de celles-ci.
    • Ajout d'un client (le paramètre sera un objet de la classe Client)
 */

import java.util.Objects;
import java.util.Vector;

public class Societe {
    //Attributs d'instance
    private String nom;

    //Associations
    private Vector<Client> clients = new Vector<>();

    public String getNom() {
        return nom;
    }

    private void setNom(String nom) {
        this.nom = (nom == null? "IBM": nom.toUpperCase());
    }

    public Vector<Client> getClients() {
        return clients;
    }

    //Constructeurs


    public Societe(String nom) {
        setNom(nom);
    }

    protected Societe(){
        this(null);
    }

    //Metier
    public void addClient(Client c){
        //Controle si c est null
        if(c==null)return;
        //Controle si c existe deja dans clients
        if(clients.contains(c))return;

        //Ajout dans le vecteur
        this.clients.addElement(c);

        //Ajout de la societe dans client
            //this.clients.elementAt(this.clients.indexOf(c)).setNomSociete(this.nom);
        c.setNomSociete(this.nom);

    }

    //equals et hash
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Societe societe = (Societe) o;
        return nom.equals(societe.nom);
    }

    @Override
    public int hashCode() {
        return Objects.hash(nom);
    }

    //toString
    @Override
    public String toString() {
        String str = "********** "  + nom + " *********\nListe des clients:\n";
        for(Client c : this.clients){
            str = str + "\t\t" + c.toString() + "\n";
        }
        return str;
    }

    public static void main(String[] args) {
        Societe SOC = new Societe("Toutou POIL");
        Client c1 = new Client("Anatole","Talent",null);
        System.out.println(c1);
        Client c2 = new Client("Miroslav","Bulldosex",null);
        System.out.println(c2);

        SOC.addClient(c1);
        System.out.println(SOC);
        SOC.addClient(c2);
        System.out.println(SOC);
        SOC.addClient(c2);
        System.out.println(SOC);


    }
}
