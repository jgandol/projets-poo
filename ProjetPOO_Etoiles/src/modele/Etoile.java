package modele;


public class Etoile {

    public static void main(String[] args) {

        int n =10;

        //Version 1
        for(int i=1; i<=n;i++){
            for(int j=0; j<n-i; j++){
                System.out.print(" ");
            }
            for(int j=0; j<i; j++){
                System.out.print("*");
            }

            System.out.println("");
        }


        //Version 2
        for(int i=0; i<=n;i++){
            for(int j=0; j<n-i; j++){
                System.out.print("*");
            }

            for(int j=0; j<=i; j++){
                System.out.print(" ");
            }

            System.out.println("");
        }

        System.out.println("");
        //Version 3

        for(int i=1; i<=n;i++){
            for(int j=0; j<n-i; j++){
                System.out.print(" ");
            }
            for(int k=0; k<i; k++){
                System.out.print("*");
            }

            System.out.print(" ");

            for(int j=0; j<n-i+1; j++){
                System.out.print("*");
            }

            for(int l=0; l<=i; l++){
                System.out.print(" ");
            }


            System.out.println(" ");
        }

    }
}
