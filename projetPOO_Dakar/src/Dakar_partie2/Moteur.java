package Dakar_partie2;




public class Moteur {
    //variables d'instance
    private int puissance;
    private char carburant;

    //Constructeur par défaut
    public Moteur() {
        setCarburant('E');
        setPuissance(1000);
    }

    //Constructeur spec

    public Moteur(int puissance, char carburant) {
        this.setPuissance(puissance);
        this.setCarburant(carburant);
    }


    // getter / setter

    public int getPuissance() {
        return puissance;
    }

    public void setPuissance(int puissance) {
        this.puissance = (puissance <= 1000 ? 1000 : puissance);
    }

    public char getCarburant() {
        return carburant;
    }

    public void setCarburant(char carburant) {
        this.carburant = (carburant != 'E' && carburant != 'D'? 'E' : carburant);
    }

    @Override
    public String toString() {
        return "Moteur{" +
                "puissance=" + puissance +
                ", carburant=" + carburant +
                '}';
    }

    //test unitaire
    public static void main(String[] args) {
        Moteur m1 = new Moteur( 1200, 'D');
        Moteur m2 = new Moteur(1600,'F');

        System.out.println(m1);
    }

}
