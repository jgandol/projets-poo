package Dakar_partie2;

public class Rallye extends Voiture {
    //Variables d'instance
    private int nbCourses;

    //Getter et setter
    public double getNbCourses() {
        return nbCourses;
    }

    public void setNbCourses(int nbCourses) {
        this.nbCourses = (nbCourses < 1? 0: nbCourses);
    }

    //Constructeurs
    public Rallye(Moteur m, String marque, int nbCourses) {
        super(m, marque);
        this.setNbCourses(nbCourses);
    }

    public Rallye(int p, char c, String marque, int nbCourses) {
        super(p, c, marque);
        this.setNbCourses(nbCourses);
    }

    public Rallye(int nbCourses) {
        this.setNbCourses(nbCourses);
    }

    //toString

    @Override
    public String toString() {
        return super.toString() +
                "\n\tnombre de courses gagnées=" + nbCourses;
    }

    public static void main(String[] args) {
        Moteur m = new Moteur();
        Voiture v = new Rallye(m,"Toyota",20);
        System.out.println(v);
    }
}
