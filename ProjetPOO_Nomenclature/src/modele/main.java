package modele;

public class main {
    public static void main(String[] args) throws ExceptionMetier {
        Produit roue    = new ProduitBrut("roue", 2.5);
        Produit chassis = new ProduitSemiFini("chassis");
        Produit axe     = new ProduitBrut("axe", 1.10);
        Produit cadre   = new ProduitSemiFini("cadre");
        Produit velo    = new ProduitFini("velo");
        Produit frein   = new ProduitBrut("frein", 2.99);
        Produit selle   = new ProduitBrut("selle", 9.75);
        Produit guidon  = new ProduitSemiFini("guidon");

        chassis.addComposants(roue, 4, "lille");
        chassis.addComposants(axe, 2, "lille");

        cadre.addComposants(roue, 2, "ascq");
        cadre.addComposants(selle, 1, "ascq");
        guidon.addComposants(frein, 2, "ascq");
        velo.addComposants(guidon, 1, "ascq");
        velo.addComposants(cadre, 1,"ascq");

        roue.ruptureStock();

        velo.afficher();
    }
}
