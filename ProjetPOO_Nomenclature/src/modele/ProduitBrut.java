package modele;

public class ProduitBrut extends Produit {
    //ATTRIBUTS D'INSTANCE
    private double prix;

    //ASSOCIATIONS

    ///////////////////////////////////// C O N S T R U C T E U R S ////////////////////////////////////////////////////

    public ProduitBrut(String nom, double prix) {
        super(nom);
        this.setPrix(prix);
    }


    //////////////////////////////// G E T T E R S      &      S E T T E R S ///////////////////////////////////////////

    @Override
    public double getPrix() {
        return prix;
    }

    private void setPrix(double prix) {
        this.prix = (prix<=0?1.0:prix);
    }


    ///////////////////////////////////////////// M E T I E R S ////////////////////////////////////////////////////////

    @Override
    protected boolean estCompatible(Produit pr) {
        return false;
    }


    // toString
    @Override
    public String toString() {
        return "ProduitBrut{" + this.getNom() +
                ", prix=" + prix +
                '}';
    }

}
