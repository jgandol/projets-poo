package Dakar_correction;

public class Voiture {
    //Variable de classe
    private static int derniereImma = 1;

    //Variables d'instance
    private int imma;
    private String marque;

    //Composition
    private Moteur moteur = new Moteur();

    //Association
    private Personne conducteur = null;


    //Constructeurs
    public Voiture() {
        this(1000,'E',null);
    }

    public Voiture(int p, char c, String marque) {
        this.moteur = new Moteur(p,c);
        this.setImma();
        this.setMarque(marque);
    }

    public Voiture(Moteur m, String marque, Personne conducteur) {
        /* A cause de la composition on risque le bug */
        if(moteur != null){
            this.moteur = new Moteur(m.getPuissance(), m.getCarburant());
        }
        this.setImma();
        this.setMarque(marque);
        this.conducteur = conducteur;
    }

    public Voiture(Moteur m, String marque) {
        /* A cause de la composition on risque le bug */
        if(moteur != null){
            this.moteur = new Moteur(m.getPuissance(), m.getCarburant());
        }
        this.setImma();
        this.setMarque(marque);
    }

    //Metier
    public boolean estDisponible(){
        return (this.conducteur == null);
    }

    //Getters et setters
    public Moteur getMoteur() {
        return this.moteur;
    }

    public int getImma() {
        return imma;
    }

    public String getMarque() {
        return marque;
    }


    public void setMoteur(Moteur moteur) {
        this.moteur = moteur;
    }

    private static void setDerniereImma() {
        Voiture.derniereImma++;
    }

    private void setImma() {
        Voiture.setDerniereImma();
        this.imma = Voiture.derniereImma;
    }

    private void setMarque(String marque) {
        this.marque = ( marque == null || marque.trim().equals("")? "FIAT":marque.toUpperCase());
    }


    public Personne getConducteur() {
        return conducteur;
    }

    public void setConducteur(Personne conducteur) {
        this.conducteur = conducteur;
    }

    @Override
    public String toString() {
        return "Voiture{" +
                "moteur=" + moteur +
                ", imma=" + imma +
                ", marque='" + marque + '\'' +
                ", conducteur = " + (conducteur==null? "Disponible" :getConducteur().getNom() + " " + getConducteur().getPrenom()) +
                '}';
    }

    public static void main(String[] args) {
        Personne p = new Personne("Doe","John");
        Voiture v1 = new Voiture(1200,'D',"Peugeot");
        Moteur m1 = new Moteur(5000,'E');
        Voiture v2 = new Voiture(m1,"Renault",p);
        Voiture v3 = new Voiture();
        System.out.println(v1);
        System.out.println(v2);
        System.out.println(v3);
    }
}
