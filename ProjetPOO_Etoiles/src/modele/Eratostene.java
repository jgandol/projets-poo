package modele;

import java.util.Scanner;

public class Eratostene {

    public static void main(String[] args) {

        System.out.print(" Entrez le nombre max : ");
        Scanner Input = new Scanner(System.in);
        int n = Input.nextInt();

        int[] nbPremier = new int[n];
        //Initialisation tableau
        for (int i = 0; i < n; i++) {
            nbPremier[i] = i + 1;
        }

        //Boucle qui parcourt le tableau
        for (int i = 1; i<nbPremier.length; i++) {
            if (nbPremier[i] != -1) {

                //Boucle qui va tester le modulo d'un case avec toutes les autres et les remplacer par un 1
                for (int j = i+1; j < nbPremier.length; j++) {
                    if (nbPremier[j] % nbPremier[i] == 0) {
                        nbPremier[j] = -1;
                    }
                }
            }
        }

        Afficher(nbPremier);
    }

    private static void Afficher(int[] nbPremier) {
        int cpt = 0;
        System.out.println("Nombres premiers :");
        System.out.print(" ");
        for (int k = 0; k < nbPremier.length-1; k++) {
            if(nbPremier[k] != -1 && cpt<7){
                System.out.print(nbPremier[k]+" ,");
                cpt++;
            } else if(cpt == 7 && nbPremier[k] != -1) {
                System.out.print(nbPremier[k] + " \n ");
                cpt = 0;
            }

        }
    }
}
