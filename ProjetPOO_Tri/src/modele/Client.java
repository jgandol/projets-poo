package modele;

import java.util.*;

public class Client {
    /////////////////////////// A T T R I B U T E S \\\\\\\\\\\\\\\\\\\\\\\\\\\
    // Class Attributes

    // Instance Attributes
    private List<Personne> ens = new ArrayList<>();


    /////////////////// G E T T E R S    &    S E T T E R S \\\\\\\\\\\\\\\\\\\\

    ///////////////////////// C O N S T R U C T O R S \\\\\\\\\\\\\\\\\\\\\\\\\\

    ////////////////////// B U S I N E S S    R U L E S \\\\\\\\\\\\\\\\\\\\\\\\
    public void ajoutPersonne(String nom, String prenom, Double salaire){
        Personne pers = new Personne(nom, prenom, salaire);
        ens.add(pers);

    }

    public void afficherPatronymeCroissant(){
        List<Personne> aux = new LinkedList<Personne>(this.ens);
        Collections.sort(aux, new PersonneComparator());

        for(Personne p :aux){
            System.out.println(p);
        }
    }

    public void afficherPatronymeDecroissant(){
        List<Personne> aux = new LinkedList<Personne>(this.ens);
        Collections.sort(aux, Collections.reverseOrder());

        for(Personne p :aux){
            System.out.println(p);
        }
    }

    public void afficherSalaireCroissant(){
        List<Personne> aux = new LinkedList<Personne>(this.ens);
        Collections.sort(aux, new PersonneComparator());

        for(Personne p :aux){
            System.out.println(p);
        }
    }

    public void afficherSalaireDecroissant(){
        class PersonneComparatorDec implements Comparator<Personne>{
            @Override
            public int compare(Personne un, Personne deux){
                int aux = un.getSalaire().compareTo(deux.getSalaire());

                if(aux !=0 )
                    return -aux;
                else
                    return -un.getIdentite().compareTo(deux.getIdentite());
            }
        }


        List<Personne> aux = new LinkedList<Personne>(this.ens);
        Collections.sort(aux, new PersonneComparatorDec());

        for(Personne p :aux){
            System.out.println(p);
        }
    }



    //////////////////// O V E R R O D E    M E T H O D S \\\\\\\\\\\\\\\\\\\\\\

    // toString
}
