package Dakar_partie2;

public class Pilote extends Personne {
    //attributs d'instance
    private String sponsor;

    //Getter/setter
    public String getSponsor() {
        return sponsor;
    }

    private void setSponsor(String sponsor) {
        this.sponsor = (sponsor == null? "redbull" : sponsor.toLowerCase());
    }

    //Constructeurs
    public Pilote(String nom, String prenom, String sponsor) {
        super(nom, prenom);
        this.setSponsor(sponsor);
    }


    public Pilote(String nom, String prenom) {
        this(nom, prenom,null);
    }

    public Pilote() {
        this.setSponsor("redbull");
    }

    //regles metier
    @Override
    protected boolean estCompatible(Voiture v) {
        if(v==null)return false;
        //return (v instanceof Camion);
        return v.getClass() == Rallye.class;
    }

    //toString
    @Override
    public String toString() {
        return  super.toString() +
                "/ sponsor= '" + sponsor + '\'' + "\n";
    }

    //main
    public static void main(String[] args) {
        /*Personne p = new Technicien("Afeux","Pierre");
        Voiture v = new Voiture(5000,'D',"tesla");
        Voiture c = new Camion(2000,'D', "CROwn", 10.0);

        p.affecter(v);
        System.out.println(p);

        p.affecter(c);
        System.out.println(p);*/
    }

}
