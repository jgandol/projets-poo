package Dakar_correction;


public class Personne {
    //Attributs de classe
    private static int dernierMatricule =0;

    //Attributs d'instance
    private int matricule;
    private String nom;
    private String prenom;

    //Association
    private Voiture voiture = null; // je nais pieton


    //Constructeurs
    public Personne() {
        this(null,null);
    }

    public Personne(String nom, String prenom) {
        Personne.dernierMatricule++;
        this.matricule = Personne.dernierMatricule;
        this.setNom(nom);
        this.setPrenom(prenom);
    }

    //Metier

    private boolean estPieton(){
        return (this.voiture == null);
    }

    public void affecter(Voiture v){
        //Cas 1: La voiture doit exister
        if( v == null)return ;

        //Cas 2: la voiture n'est pas disponible
        if( ! v.estDisponible()) return;

        //Cas 3: je ne suis pas pieton
        if( ! this.estPieton()) return;

        //Action 1: la voiture a un conducteur
        v.setConducteur(this); //MOI

        //Action 2 : Je suis conducteur
        this.setVoiture(v);
    }

    public void restituer(){

        //Cas 1: je dois etre pieton
        if( this.estPieton()) return;

        //Action 1: La voiture redevient dispo
        this.getVoiture().setConducteur(null);

        //Action 2: Je suis pieton
        this.setVoiture(null);

    }

    //Matricule, nom, prenom sont immuables
    public int getMatricule() {
        return matricule;
    }

    public String getNom() {
        return nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public Voiture getVoiture() {
        return voiture;
    }

    public void setVoiture(Voiture voiture) {
        this.voiture = voiture;
    }

    public void setMatricule(int matricule) {
        this.matricule = matricule;
    }

    public void setNom(String nom) {
        this.nom = (nom==null || nom.trim().equals("")?"ENFAYITE":nom.toUpperCase());
    }

    public void setPrenom(String prenom) {
        this.prenom = (prenom==null || prenom.trim().equals("")?"MELUSINE":prenom.toUpperCase());
    }


    @Override
    public String toString() {
        return "Personne{" +
                "matricule=" + matricule +
                ", nom='" + nom + '\'' +
                ", prenom='" + prenom + '\'' +
                ", voiture= " + (voiture ==null?"Je suis pieton":voiture) +
                '}';
    }

    public static void main(String[] args) {

        Personne luc = new Personne("luc","hiluke");
        Voiture v1 = new Voiture(5000,'E',"MITSUBISHI");
        System.out.println(luc);
        luc.affecter(v1);
        System.out.println(luc);
        luc.restituer();
        System.out.println(luc);
    }
}
