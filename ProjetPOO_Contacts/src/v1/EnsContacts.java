package v1;/*
TODO: Les opérations de cette classe seront :
    • Constructeur par défaut
    • Affichage des noms des sociétés
    • Affichage des clients d'une société donnée (le paramètre sera le nom de cette société)
    • Ajout d'un client (les paramètres seront les noms du client et de sa société)
 */

import java.util.Enumeration;
import java.util.Hashtable;

public class EnsContacts {
    //Attributs d'instance
    private String nom;

    //Associations
    private Hashtable<String, Societe> societes = new Hashtable<>();

    //Getters et Setters
    public String getNom() {
        return nom;
    }

    private void setNom(String nom) {
        this.nom = (nom==null?"Carnet d'adresses":nom);
    }

    public Hashtable<String, Societe> getSocietes() {
        return societes;
    }

    //Metier

    public void ajout(String nomClient, String prenom, String nomSoc){
        Client c = new Client(nomClient,prenom,nomSoc);
        String cle = c.getNomSociete();

        creerSociete(cle); // creation conditionnelle

        Societe soc = this.societes.get(cle);
        soc.addClient(c);

    }

    private void creerSociete(String nom){
        String cle = (nom==null?"IBM" : nom.toUpperCase());
        if(this.societes.containsKey(cle))return;

        this.societes.put(cle, new Societe(cle));
    }

    public void afficherNomSociete(){
        for(String nomSoc : this.societes.keySet()){
            System.out.println(nomSoc);
        }
    }

    public void afficherNomSociete2(){
        Enumeration<String> enumCle = this.societes.keys();
        while(enumCle.hasMoreElements()){
            String cle = enumCle.nextElement();
            System.out.println(cle);
        }
    }

    public void afficherTout(){
        Enumeration<String> enumCle = this.societes.keys();
        while(enumCle.hasMoreElements()){
            String cle = enumCle.nextElement();
            System.out.println(cle);

            Societe soc = this.societes.get(cle);
            Enumeration<Client> enumcl = soc.getClients().elements();
            while(enumcl.hasMoreElements()){
                Client cl = enumcl.nextElement();
                System.out.println("\t"+cl.toString());
            }
        }
    }

    public void afficherTout2(){
        for(String cle : this.societes.keySet()){
            System.out.println(cle);
            Societe soc = this.societes.get(cle);

            for(Client cl: soc.getClients()){
                System.out.println("\t"+cl.toString());
            }
        }
    }

    //Constructeur

    public EnsContacts() {
        setNom(null);
    }


    public static void main(String[] args) {
        EnsContacts carnet = new EnsContacts();
        carnet.ajout("Pelouse","Thierry","Toyota");
        carnet.ajout("Talent","Anatole","Toyota");
        carnet.ajout(null,null,"Toutou Poil");

        carnet.afficherNomSociete();
        System.out.println();
        carnet.afficherTout();
    }
}

