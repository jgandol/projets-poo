package modele;

public class Departement extends Zone {
    private String prefet;


    //GET SET
    public String getPrefet(){
        return prefet;
    }

    public void setPrefet(String prefet){
        this.prefet = (prefet ==null?"BORLOO": prefet.toUpperCase());
    }

    //CONSTRUCT
    public Departement(String nom, String prefet) {
        super(nom);
        setPrefet(prefet);
    }

    public Departement(){
        this(null,null);
    }

    @Override
    public String toString() {
        return super.toString() + "\n\tprefet='" + prefet + '\'' +
                '}';
    }
}
