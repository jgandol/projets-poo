package modele;

public class Region extends Zone {
    private String responsable;

    //GET SET
    public String getResponsable(){
        return responsable;
    }

    public void setResponsable(String responsable){
        this.responsable = (responsable ==null?"BERTRAND": responsable.toUpperCase());
    }

    //CONSTRUCT
    public Region(String nom, String responsable) {
        super(nom);
        setResponsable(responsable);
    }

    public Region(){
        this(null,null);
    }

    @Override
    public String toString() {
        return super.toString() + "\n\tresponsable='" + responsable + '\'' +
                '}';
    }
}
