package modele;

import java.util.*;

public class Patronyme implements Comparable<Patronyme>, Cloneable {
    private String nom;
    private String prenom;

    public String getNom() {
        return nom;
    }

    private void setNom(String nom) {
        this.nom = (nom==null?"DUPOND":nom.toUpperCase());
    }

    public String getPrenom() {
        return prenom;
    }

    private void setPrenom(String prenom) {
        this.prenom = (prenom==null?"JEAN":prenom.toUpperCase());
    }

    public Patronyme(String nom, String prenom) {
        setNom(nom);
        setPrenom(prenom);
    }

    public Patronyme(){
        this(null,null);
    }

    @Override
    public int compareTo(Patronyme patronyme) {
        int aux = this.nom.compareTo(patronyme.nom);
        if(aux !=0 )
            return aux;

        return this.prenom.compareTo(patronyme.prenom);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Patronyme patronyme = (Patronyme) o;
        return Objects.equals(nom, patronyme.nom) &&
                Objects.equals(prenom, patronyme.prenom);
    }

    @Override
    public int hashCode() {
        return Objects.hash(nom, prenom);
    }

    @Override
    public String toString() {
        return "Patronyme{" +
                "nom='" + nom + '\'' +
                ", prenom='" + prenom + '\'' +
                '}';
    }

    public static void main(String[] args) {
        Patronyme p  = new Patronyme();
        Patronyme p2 = new Patronyme("gator","nathalie");
        Patronyme p3 = new Patronyme("gator","magalie");

        Set<Patronyme> ensemble = new TreeSet<>();
        ensemble.add(p);
        ensemble.add(p2);
        ensemble.add(p3);

        System.out.println(ensemble);

        System.out.println(p);
        System.out.println(p2);
        System.out.println("Egalite: "+p.equals(p2));
        System.out.println("Egalite: "+p.equals(p));

        System.out.println("Ordre: "+p.compareTo(p2));
        System.out.println("Ordre: "+p.compareTo(p));

        Map<Patronyme, Integer> dico = new TreeMap<>();
        dico.put(p,30);
        dico.put(p2,32);
        dico.put(p3, 31);
        System.out.println(dico.keySet());
    }
}
