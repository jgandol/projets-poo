package modele;

import java.util.*;

public class Personne implements Comparable<Personne> {

    //Attribut de classe
    private static int dernierMatricule = 0;

    //Attribut d'instance
    private int matricule =1;
    private Patronyme identite;
    private Double salaire;

    //Constructeurs


    public Personne(String nom,String prenom, double salaire) {
        this.identite = new Patronyme(nom, prenom);
        setSalaire(salaire);
        setMatricule(dernierMatricule);
    }

    public Personne(){
        this(null,null,0.0);
    }

    public int getMatricule() {
        return matricule;
    }

    public void setMatricule(int matricule) {
        Personne.dernierMatricule++;
        this.matricule = Personne.dernierMatricule;
    }

    public Patronyme getIdentite() {
        return identite;
    }

    public void setIdentite(Patronyme identite) {
        this.identite = identite;
    }

    public Double getSalaire() {
        return salaire;
    }

    public void setSalaire(double salaire) {
        this.salaire = (salaire < 1000.0 ? 1000.0 : salaire);
    }




    @Override
    public int compareTo(Personne autre) {
        return identite.compareTo(autre.identite);
    }



    @Override
    public int hashCode() {
        return Objects.hash(identite);
    }

    @Override
    public String toString() {
        return "Personne{" +
                "matricule=" + matricule +
                ", identite=" + identite +
                ", salaire=" + salaire +
                '}';
    }

    public static void main(String[] args) {
        Personne p1 = new Personne();
        Personne p2 = new Personne("afeux", "Pierre",1200.0);
        Personne p3 = new Personne("afeux", "Magalie",1800.0);
        Set<Personne> ens = new TreeSet<>();
        ens.add(p1);
        ens.add(p2);
        ens.add(p3);

        System.out.println(ens);

        System.out.println("--------------------");
        Set<Personne> ens2 = new TreeSet<>(new PersonneComparator());
        ens2.add(p1);
        ens2.add(p2);
        ens2.add(p3);


        System.out.println(ens2);
    }
}
