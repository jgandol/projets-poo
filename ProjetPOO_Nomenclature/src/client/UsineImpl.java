package client;

import modele.*;

import java.util.Map;
import java.util.TreeMap;

public class UsineImpl implements Usine{
    private Map<String, Produit> produits = new TreeMap<>();


    @Override
    public void creerProduit(enumProduit nature, String nomProduit) {
        if(nomProduit == null)return;

        if(this.produits.containsKey(nomProduit.toUpperCase())){
            System.err.println("Le produit existe deja");
            return;
        }

        Produit p = null;

        switch (nature) {
            case fini:
                p = new ProduitFini(nomProduit);
                System.out.println("Produit fini");
                break;
            case semi_fini:
                p = new ProduitSemiFini(nomProduit);
                System.out.println("Produit semi fini");
                break;
            case brut:
                p = new ProduitBrut(nomProduit, 1.0);
                System.out.println("Produit brut");
                break;
        }

        this.produits.put(nomProduit.toUpperCase(), p);

    }

    public void creerProduit(enumProduit nature, String nomProduit, double prix) {
        if(nomProduit == null)return;
        if(nature != enumProduit.brut) System.out.println("Le produit doit être brut");

        if(this.produits.containsKey(nomProduit.toUpperCase())){
            System.err.println("Le produit existe deja");
            return;
        }

        Produit pBrut = new ProduitBrut(nomProduit, prix);
        this.produits.put(nomProduit.toUpperCase(), pBrut);

    }

    @Override
    public void ajoutComposant(String nomProduitOrigine, String nomProduitDestination, int quantite, String lieu) {
        if(nomProduitDestination == null) return;
        Produit produitDestination = this.produits.get(nomProduitDestination.toUpperCase());

        if(nomProduitOrigine == null) return;
        Produit produitOrigine = this.produits.get(nomProduitOrigine.toUpperCase());

        if(produitDestination == null)return;

        try {
            produitDestination.addComposants(produitOrigine,quantite,lieu);
        } catch (ExceptionMetier exceptionMetier) {
            return;
        }

    }

    @Override
    public double getPrix(String nomProduit) {
        if(nomProduit==null) return 0.0;
        Produit produit = this.produits.get(nomProduit.toUpperCase());

        if(produit == null)return 0.0;
        return produit.getPrix();
    }

    @Override
    public void afficherNomenclature(String nomProduit) {
        if(nomProduit == null)return;
        if(this.produits.containsKey(nomProduit.toUpperCase())){
            Produit pr = this.produits.get(nomProduit.toUpperCase());
            pr.afficher();
        }

    }

    @Override
    public void ruptureStock(String nomProduit) {
        if(nomProduit == null)return;
        if(this.produits.containsKey(nomProduit.toUpperCase())){
            Produit pr = this.produits.get(nomProduit.toUpperCase());
            pr.ruptureStock();
        }

    }

}
