package client;

public interface Usine {

    void creerProduit(enumProduit nature, String nomProduit);
    void creerProduit(enumProduit nature, String nomProduit, double prix);

    void ajoutComposant(String nomProduitOrigine, String nomProduitDestination, int quantite, String lieu);

    double getPrix(String nomProduit);

    void afficherNomenclature(String nomProduit);

    void ruptureStock(String nomProduit);

}
