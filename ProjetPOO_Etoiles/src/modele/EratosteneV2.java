package modele;


public class EratosteneV2 {

    public static void main(String[] args) {

        int crt = 2, n=100;
        boolean isPremier;
        boolean[] tab = new boolean[n];

        do{
            for(int i=crt*2; i<n; i+=crt){
                tab[i]= false;
            }
            isPremier = false;

            for(int i=crt+1; i<=n-1; i++){
                if(tab[i]){
                    isPremier = true;
                    crt = i;
                    break;
                }
            }
        }while (isPremier);

        int compteur = 0;
        for(int i = 1; i<=n-1;i++){
            if(tab[i]){
                compteur++;

                String str = String.valueOf(100);
                String modele = "%"+String.valueOf(str.length()-1)+"d";
                System.out.printf(modele,i);

                System.out.printf(" %2d",i);

                if(compteur%8 == 0){
                    System.out.println();
                }
            }
        }
    }


}
