package client;

public class Main {
    public static void main(String[] args) {
        Usine usine = new UsineImpl();

        usine.creerProduit(enumProduit.brut, "roue",2); // bon
        usine.creerProduit(enumProduit.brut, "axe",5); // bon
        usine.creerProduit(enumProduit.brut, "frein",3.7); // bon
        usine.creerProduit(enumProduit.brut, "selle",10);// bon
        usine.creerProduit(enumProduit.semi_fini, "guidon");
        usine.creerProduit(enumProduit.semi_fini, "Cadre");
        usine.creerProduit(enumProduit.fini, "velo");

        usine.ajoutComposant("frein","guidon",2,"Lille");
        usine.ajoutComposant("selle","cadre",1,"Lille");
        usine.ajoutComposant("roue","cadre",2,"Lille");

        usine.ajoutComposant("guidon","velo",1,"Lille");
        usine.ajoutComposant("cadre","velo",1,"Lille");

        System.out.println(usine.getPrix("guidon"));

        usine.afficherNomenclature("velo");
        usine.ruptureStock("frein");

    }
}
