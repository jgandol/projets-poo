package modele;

public class Commune extends Zone {
    private String maire;
    private int population;

    //GET SET
    public String getMaire(){
        return maire;
    }

    public void setMaire(String maire){
        this.maire = (maire ==null?"BERTRAND": maire.toUpperCase());
    }

    public void setPopulation(int population) {
        this.population = population;
    }

    @Override
    public int getPopulation() {
        return population;
    }

    //CONSTRUCT
    public Commune(String nom, String maire, int population) {
        super(nom);
        this.setMaire(maire);
        this.setPopulation(population);
    }

    public Commune(){
        this(null,null,0);
    }

    @Override
    public String toString() {
        return super.toString() + "\n\tmaire='" + maire + '\'' +
                ", population=" + population +
                '}';
    }
}
