package modele;

import java.util.Comparator;

public class PersonneComparator implements Comparator<Personne> {
    @Override
    public int compare(Personne p1, Personne p2) {
        int aux = p1.getSalaire().compareTo((p2.getSalaire()));

        if(aux !=0 )
            return aux;
        else
            return p1.getIdentite().compareTo(p2.getIdentite());

    }
}
