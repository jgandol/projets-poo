package modele;

public class ProduitSemiFini extends Produit {

    ///////////////////////////////////// C O N S T R U C T E U R S ////////////////////////////////////////////////////
    public ProduitSemiFini(String nom) {
        super(nom);
    }

    ///////////////////////////////////////////// M E T I E R S ////////////////////////////////////////////////////////
    @Override
    protected boolean estCompatible(Produit pr) {
        if(pr == null) return false;
        return (pr instanceof ProduitBrut);
    }

    //toString
    @Override
    public String toString() {
        return "ProduitSemiFini{" + super.toString() + "}";
    }
}