package team;

import Dakar_partie2.*;

public class Equipe {
    //attributs d'instance
    private String nom;

    public String getNom() {
        return nom;
    }

    private void setNom(String nom) {
        this.nom = (nom == null? "FRANCE":nom.toUpperCase());
    }


    //Association 1 <--> 5
    private int nbPersonnes = 0;
    private Personne[] personnes = new Personne[6];

    private int nbVoitures = 0;
    private Voiture[] parc = new Voiture[6];

    //Constructeur
    public Equipe(String nom) {
        this.setNom(nom);
    }

    //  G e s t i o n    d e s    P e r s o n n e s
    public void embauche(Personne p){

        // controle d'existance
        if(p==null) return;

        //a t on atteint la limite
        if(this.nbPersonnes == 5) return;

        //controle d'absence de la nouvelle personne
        if(this.estPresent(p)) return;

        // maj du tableau du personnel
        this.nbPersonnes++;
        this.personnes[this.nbPersonnes] = p;
    }

    private int localiser(Personne p){
        if(p == null)return 0;

        //utiliser une sentinelle
        this.personnes[0] = p;
        int i = this.nbPersonnes;
        int cle = p.getMatricule();
        while (this.personnes[i].getMatricule() != cle){
            i = i-1;
        } return i;
    }

    private boolean estPresent(Personne p){
        return (this.localiser(p)>0); //then TRUE else FALSE
    }

    //  G e s t i o n    d e s    V o i t u r e s

    //parc pas plein, voiture existe, pas présente
    public void acheter(Voiture v){

        //Voiture existe
        if(v==null)return;

        //Parc n'est pas plein
        if(this.nbVoitures == 5)return;

        //Controle d'absences
        if(estPresent(v))return;

        //Affectation
        this.nbVoitures++;
        this.parc[this.nbVoitures] = v;
    }

    private int localiser(Voiture v) {
        if (v == null) return 0;
        this.parc[0] = v;
        int i = nbVoitures;
        int cle = v.getImma();
        while(this.parc[i].getImma() != cle)i--;
        return i;
    }

    private boolean estPresent(Voiture v){
        return (this.localiser(v)>0);
    }

    // R e g l e s   M e t i e r

    public void affecter(Personne p, Voiture v) {
        int positionP = this.localiser(p);
        int positionV = this.localiser(v);
        if (positionP==0) return;
        if (positionV==0) return;

        this.personnes[positionP].affecter(this.parc[positionV]);

    }

    public void restituer(Personne p){
        int positionP = this.localiser(p);
        if(positionP == 0) return;

        this.personnes[positionP].setVoiture(null);
    }

    @Override
    public String toString() {


        String str = "\nEquipe:" + " nom= " + nom + "\n";
        str = str + "Personnel";
        for(int i=1; i<= this.nbPersonnes; i++){
            str = str + "\n\t" + personnes[i];
        }

        str = str + "Parc";
        for(int i=1; i<= this.nbVoitures; i++){
            str = str + "\n\t" + parc[i];
        }

        return str + "\n}";

    }

    public static void main(String[] args) {
        Personne t = new Technicien();
        Personne p = new Pilote();
        Voiture c = new Camion(new Moteur(),"Honda",10);
        Voiture r = new Rallye(new Moteur(),"Toyota",50);
        Equipe equipe = new Equipe("Norauto");

        System.out.println(equipe);
        equipe.acheter(c);
        equipe.acheter(r);
        equipe.embauche(t);
        equipe.embauche(p);
        System.out.println(equipe);
        equipe.affecter(p,r);
        equipe.affecter(t,c);
        System.out.println(equipe);

        equipe.restituer(t);
        equipe.restituer(p);

        System.out.println(equipe);

    }
}
