package Voiture;

public class Personne {
    private static int dernier_matricule =0;
    private int matricule;
    private String nom;
    private String prenom;
    public String adresse;
    private Voiture voiture;
    private boolean estPieton = true;


    public Personne(Voiture voiture, String nom, String prenom, String adresse) {
        this.voiture = voiture;
        setNom(nom);
        this.prenom = prenom;
        this.adresse = adresse;
        setMatricule();
        setEstPieton();
    }

    public Personne(String nom, String prenom, String adresse) {
        setNom(nom);
        this.prenom = prenom;
        this.adresse = adresse;
        setMatricule();
        setEstPieton();
    }

    public Personne(){
        this(null," "," "," ");
        setEstPieton();
    }

    public void affecter(Voiture v, Personne p){
        if ( v == null){
            System.out.println("Voiture inexistante");
        } else if ( v.getEstDisponible() ==  false){
            System.out.println("la voiture n'est pas disponible");
        } else if ( this.getEstPieton() == false){
            System.out.println("La personne possède deja une voiture");
        } else {
            v.setConducteur(p);
            p.setVoiture(v);
            v.setEstDisponible(false);
            p.setEstPieton();
            System.out.println(p.getEstPieton());
        }
    }

    public void restituer(Personne p){
        p.setEstPieton();
        System.out.println(p.getEstPieton());
        if(p.getEstPieton() == true){
            System.out.println("La personne ne possède pas de voiture");
        } else {
            p.getVoiture().setEstDisponible(true);
            p.getVoiture().setConducteur(null);
            p.setVoiture(null);
            p.setEstPieton();
        }

    }

    public boolean getEstPieton() {
        return estPieton;
    }

    public void setEstPieton() {
        if(this.voiture != null){
            this.estPieton = false;
        } else {
            this.estPieton = true;
        }

    }

    public String getNom() {
        return nom;
    }

    private void setNom(String nom) {
        this.nom = ( nom == null || nom.trim().equals("")? nom :nom.toUpperCase());
    }

    public String getPrenom() {
        return prenom;
    }

    private void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public int getMatricule() {
        return matricule;
    }

    private void setMatricule() {
        Personne.setDernier_matricule();
        this.matricule = Personne.dernier_matricule;
    }

    private static void setDernier_matricule(){
        Personne.dernier_matricule++;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public Voiture getVoiture() {
        return voiture;
    }

    private void setVoiture(Voiture voiture) {
        this.voiture = voiture;
    }

    @Override
    public String toString() {
        return "Personne{" +
                "matricule=" + matricule +
                ", nom='" + nom + '\'' +
                ", prenom='" + prenom + '\'' +
                ", adresse='" + adresse + '\'' +
                ", voiture=" + voiture +
                ", estPieton=" + estPieton +
                '}';
    }

    public static void main(String[] args) {
        Moteur m = new Moteur(2200,'E');

        Voiture v = new Voiture(1500,'D',"Toyota");
        Voiture v2 = new Voiture(5000,'E',"Mitsubishi");

        Personne p = new Personne();
        Personne p2 = new Personne(v2,"Pelouse","Thierry","2 rue du Compte");

        System.out.println(p);
        System.out.println(p2);

        Personne p3 = new Personne (null,"Bulldosex","Miroslav","3 rue de la liberté");
        Voiture v3 = new Voiture(m,"Ferrari");

        p3.affecter(v3,p3);

        System.out.println(p3);
        p.restituer(p);
        System.out.println(p3);
    }
}
