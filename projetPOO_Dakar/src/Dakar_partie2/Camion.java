package Dakar_partie2;

public class Camion extends Voiture {
    //Variables d'instance
    private double tonnage;

    //Getter et setter
    public double getTonnage() {
        return tonnage;
    }

    public void setTonnage(double tonnage) {
        this.tonnage = (tonnage < 5.0? 5.0:tonnage);
    }

    //Constructeurs
    public Camion(Moteur m, String marque, double tonnage) {
        super(m, marque);
        this.setTonnage(tonnage);
    }

    public Camion(int p, char c, String marque, double tonnage) {
        super(p, c, marque);
        this.setTonnage(tonnage);
    }

    public Camion(double tonnage) {
        this.setTonnage(tonnage);
    }

    //toString

    @Override
    public String toString() {
        return super.toString() +
                "\n\ttonnage=" + tonnage;
    }

    public static void main(String[] args) {
        Moteur m = new Moteur();
        Voiture v = new Camion(m,"Toyota",200);
        System.out.println(v);
    }
}
