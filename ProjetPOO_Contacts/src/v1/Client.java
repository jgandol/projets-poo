package v1;/*  TODO:
     • Constructeur spécialisé
     • Accès en lecture et/ou écriture aux divers attributs (inspecteurs, modificateurs)
     • Affichage d'un client (toString)
     • Egalité de deux clients basée sur la comparaison des noms de ceux-ci.*/

import java.util.Objects;
import java.util.Vector;

public class Client {

    //Attributs d'instance
    private String nom; //immuable -> private
    private String prenom; //immuable -> private
    private String nomSociete; //muable -> public

    //Getter / Setter
    public String getNom() {
        return nom;
    }

    private void setNom(String nom) {
        this.nom = (nom==null?"Pelouse":nom.toUpperCase());
    }

    public String getPrenom() {
        return prenom;
    }

    private void setPrenom(String prenom) {
        this.prenom = (prenom==null?"Thierry":prenom.toUpperCase());
    }

    public String getNomSociete() {
        return nomSociete;
    }

    public void setNomSociete(String nomSociete) {
        this.nomSociete = (nomSociete==null?"IBM":nomSociete.toUpperCase());
    }

    //Constructeurs
    public Client(String nom, String prenom, String nomSociete) {
        this.setNom(nom);
        this.setPrenom(prenom);
        this.setNomSociete(nomSociete);
    }

    protected Client() {
        this(null,null,null);
    }


    //equals et Hashcode

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Client client = (Client) o;
        return nom.equals(client.nom) &&
                prenom.equals(client.prenom) &&
                nomSociete.equals(client.nomSociete);
    }

    @Override
    public int hashCode() {
        return Objects.hash(nom, prenom, nomSociete);
    }


    //toString

    @Override
    public String toString() {
        return "• " + nom + " " + prenom + " de la société " + nomSociete;
    }

    public static void main(String[] args) {
        Client c = new Client("Miroslav", "Bulldosex", "TOYOTA");
        Client c2 = new Client("Miroslav", "Bulldosex", "TOYOTA");
        Client c3 = new Client();
        Client c4 = c;
        System.out.println(c);
        System.out.println(c2);
        System.out.println(c3);

        Vector<Client> ens = new Vector<>();
        ens.addElement(c); // ou ens.add(c);

        if(ens.contains(c))
            System.out.println("OUI");

        if(ens.contains(c2))
            System.out.println("OUI");
        else
            System.out.println("OOOPSi");

        if(ens.contains(c4))
            System.out.println("OUI");
        else
            System.out.println("OOOPSi");
    }


}
