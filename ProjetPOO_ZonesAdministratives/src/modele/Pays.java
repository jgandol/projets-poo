package modele;

public class Pays extends Zone {
    private String president;


    //GET SET
    public String getPresident(){
        return president;
    }

    public void setPresident(String president){
        this.president = (president ==null?"MACRON":president.toUpperCase());
    }

    //CONSTRUCT
    public Pays(String nom, String president) {
        super(nom);
        setPresident(president);
    }

    public Pays(){
        this(null,null);
    }

    @Override
    public String toString() {
        return super.toString() + "\n\tpresident=" + president + "}";
    }
}
