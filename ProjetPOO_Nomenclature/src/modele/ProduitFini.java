package modele;

public class ProduitFini extends Produit {
    public ProduitFini(String nom) {
        super(nom);
    }

    ///////////////////////////////////////////// M E T I E R S ////////////////////////////////////////////////////////


    @Override
    protected boolean estCompatible(Produit pr) {
        if(pr == null) return false;
        return (pr instanceof ProduitSemiFini);
    }

    @Override
    public String toString() {
        return super.toString();
    }
}