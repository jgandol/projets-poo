package modele;

import java.util.*;

public class Zone implements Comparable<Zone>{
    //ATTRIBUTS DE CLASSE
    private static Map<String, Zone> zones = new TreeMap<>();

    //ATTRIBUTS D'INSTANCE
    private String nom;

    //ASSOCIATION --> Mere
    private Zone mere = null;

    //ASSOCIATION --> Fille
    private Set<Zone> filles = new TreeSet<>();


    //METIER
    public int getPopulation(){
        int somme = 0;
        for(Zone f : this.filles){
            somme += f.getPopulation();
        }
        return somme;
    }

    public static void ajouterPays(String nom, String president) throws Exception {
        if(nom==null)
            throw new Exception("Zone- creation Pays - le nom est obligatoire");

        String cle = nom.toUpperCase();
        if(Zone.zones.containsKey(cle))
            throw new Exception("Zone- creation Pays - le nom doit etre unique");

        //president = (president==null?"MACRON": president.toUpperCase());

        Zone nouv = new Pays(nom, president);
        Zone.zones.put(cle, nouv);
    }

    public static void ajouterRegion (String nom, String responsable, String nomPays) throws Exception  {

        if(nom==null)
            throw new Exception("Zone- creation Region - le nom est obligatoire");
        String cle = nom.toUpperCase();
        if(Zone.zones.containsKey(cle))
            throw new Exception("Zone- creation Region - le nom doit etre unique");

        if(nomPays==null)
            throw new Exception("Zone- creation Region - le nom du Pays ne peut etre null");
        String cle2 = nomPays.toUpperCase();
        if(! Zone.zones.containsKey(cle2))
            throw new Exception("Zone- creation Region - le nom du Pays est inconnu");

        Zone zonePays = Zone.zones.get(cle2);
        if(! (zonePays instanceof Pays))
            throw new Exception("Zone - creation Region - l'objet parent n'est pas un pays");

        Zone zoneRegion = new Region(nom, responsable);
        zoneRegion.mere = zonePays;
        zonePays.filles.add(zoneRegion);

        Zone.zones.put(cle, zoneRegion);
    }

    public static void ajouterDepartement(String nom, String prefet, String nomRegion) throws Exception {

        if(nom==null)
            throw new Exception("Zone- creation Departement - le nom est obligatoire");
        String cle = nom.toUpperCase();

        if(Zone.zones.containsKey(cle))
            throw new Exception("Zone- creation Departement - le nom doit etre unique");

        if(nomRegion==null)
            throw new Exception("Zone- creation Departement - le nom du Pays ne peut etre null");
        String cle2 = nomRegion.toUpperCase();

        if(! Zone.zones.containsKey(cle2))
            throw new Exception("Zone- creation Departement - le nom du Pays est inconnu");

        Zone zoneRegion = Zone.zones.get(cle2);
        if(! (zoneRegion instanceof Region))
            throw new Exception("Zone - creation  - l'objet parent n'est pas une region");

        Zone zoneDepartement = new Departement(nom, prefet);
        zoneDepartement.mere = zoneRegion;
        zoneRegion.filles.add(zoneDepartement);

        Zone.zones.put(cle, zoneDepartement);
    }

    public static void ajouterCommune(String nom, String maire, String nomDepartement, int population) throws Exception {

        if(nom==null)
            throw new Exception("Zone- creation Commune - le nom est obligatoire");
        String cle = nom.toUpperCase();
        if(Zone.zones.containsKey(cle))
            throw new Exception("Zone- creation Commune - le nom doit etre unique");

        if(nomDepartement==null)
            throw new Exception("Zone- creation Commune - le nom du Pays ne peut etre null");
        String cle2 = nomDepartement.toUpperCase();

        if(! Zone.zones.containsKey(cle2))
            throw new Exception("Zone- creation Commune - le nom du Pays est inconnu");

        Zone zoneDepartement = Zone.zones.get(cle2);
        if(! (zoneDepartement instanceof Departement))
            throw new Exception("Zone - creation Commune - l'objet parent n'est pas une region");

        Zone zoneCommune = new Commune(nom, maire, population);
        zoneCommune.mere = zoneDepartement;
        zoneDepartement.filles.add(zoneCommune);

        Zone.zones.put(cle, zoneCommune);
    }

    public static Zone rechercher(String nom){
        if(nom==null)
            return null;

        return Zone.zones.get(nom.toUpperCase());
    }

    public static int getPop(String nom){
        Zone z = Zone.rechercher(nom);
        if(z==null)
            return -1;
        return z.getPopulation();
    }

    public static void afficher(String nom){
        System.out.println("*******   Arborescence des Zones   ******");
        Zone z = Zone.rechercher(nom);
        if(z == null)
            return;
        z.afficherBis(0);
    }

    public void afficherBis(int decalage){
        for(int i = 0; i<decalage; i++)
            System.out.print("\t");

        System.out.println(this.getClass().getSimpleName() + " " + this.getNom() + " : " + this.getPopulation());

        for(Zone f : this.filles){
            f.afficherBis(decalage+1);
        }
    }

    public static void afficherCle(){
        System.out.println("*******   N o m s   d e s   Z o n e s   ******");
        for(String cle: Zone.zones.keySet()){
            System.out.println(cle);
        }
    }

    //GETTERS & SETTERS
    public String getNom() {
        return nom;
    }

    private void setNom(String nom) {
        this.nom = (nom==null?"FRANCE":nom.toUpperCase());
    }

    public Zone getMere() {
        return mere;
    }

    private void setMere(Zone mere) {
        this.mere = mere;
    }

    public Set<Zone> getFilles() {
        return filles;
    }


    //CONSTRUCTEURS
    public Zone(String nom, Zone mere, Set<Zone> filles) {
        setNom(nom);
        this.mere = mere;
        this.filles = filles;
    }

    protected Zone(String nom){
        setNom(nom);
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName();
    }

    @Override
    public int compareTo(Zone autre) { //Redefinition de l'ordre nanturel : comparaison des noms des zones
        //return this.getPopulation().compareTo(autre);
        //retrun new Integer(this.getPopulation()).compareTo(new Integer(autre.getPopulation()));
        return (this.getPopulation() - autre.getPopulation());
    }

    public static void main(String[] args) throws Exception {

        Zone.ajouterPays("france", "macron");
        Zone.ajouterRegion("Hauts de France", "Bertrand", "france");
        Zone.ajouterRegion("ACA", "Nathanael", "france");
        Zone.ajouterDepartement("Champagne", "Thierry", "ACA");
        Zone.ajouterDepartement("nord", "Borlo", "Hauts de france");
        Zone.ajouterCommune("Tourcoing","Titine","nord",190);
        Zone.ajouterCommune("Marly","Fabien thiémé","nord",200);

        Zone c = Zone.rechercher("tourcoing");
        Zone m = Zone.rechercher("Marly");
        Zone h = Zone.rechercher("Hauts de france");
        Zone f = Zone.rechercher("france");

        Zone.afficher("France");

        Zone.afficherCle();
/*
        System.out.println(c);
        System.out.println(m);
        System.out.println(h);
        System.out.println(f);
        System.out.println(f.getPopulation());
*/



    }


}
