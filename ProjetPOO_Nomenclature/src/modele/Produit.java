package modele;

import java.util.*;

public abstract class Produit{
    //ATTRIBUT D'INSTANCE
    private String nom;


    //ASSOCIATIONS
    private Set<Fabrication> composants =
            new HashSet<>();
    private Set<Fabrication> composites =
            new HashSet<>();

    ///////////////////////////////////////////// M E T I E R S ////////////////////////////////////////////////////////

    protected abstract boolean estCompatible(Produit pr);

    public void addComposants(Produit p, int qte, String atelier) throws ExceptionMetier{
        //Contrôles
        if(p==null)
            throw new ExceptionMetier("Le produit est null");
        /*if(!(p instanceof Produit))
            throw new ExceptionMetier("Le produit n'est pas une instance de la classe Produit");
        if(this.composants.contains(p))
            throw new ExceptionMetier("Le produit existe deja");*/
        if(!this.estCompatible(p))
            throw new ExceptionMetier("Le compasant n'est pas compatible");

        Fabrication f = new Fabrication(qte, atelier, p, this);
        p.composites.add(f);
        this.composants.add(f);

    }

    public double getPrix() {
        double total = 0.0d;

        for (Fabrication f : this.composants) {
            total = total + f.quantite* f.origine.getPrix();
        }
        return total;
    }

    /*
    public String ruptureStock(){
        String str = "";
        for(Fabrication f: this.composites){
            if(this.composites.contains(this))
                System.out.println("pb: " + this.nom);
            str = str + " " + f.destination.getNom();
        }
        return str;
    }*/

    public void ruptureStock() {
        System.out.println("\n***** Produits affectés par la rupture de stock de : " + this.nom.toUpperCase() +" *****");
        sold(0);
    }

    public void sold(int decalage){
        for(int i = 0; i<decalage; i++)
            System.out.print("\t");

        System.out.println("• " + this.getClass().getSimpleName() + " : " + this.getNom());

        for (Fabrication f : this.composites) {
            f.destination.sold(decalage+1);
        }

    }



    public void afficher(){
        System.out.println("\n*******   Arborescence des produits   ******");
        afficherBis(0);
    }

    public void afficherBis(int decalage){
        for(int i = 0; i<decalage; i++)
            System.out.print("\t");

        System.out.println("• "+ this.nom+ " prix: " + this.getPrix());

        for(Fabrication f : composants){
            f.origine.afficherBis(decalage+1);
        }
    }


    ///////////////////////////////////// C O N S T R U C T E U R S ////////////////////////////////////////////////////

    public Produit(String nom) {
        this.nom = nom;
    }


    //////////////////////////////// G E T T E R S      &      S E T T E R S ///////////////////////////////////////////

    public String getNom() {
        return nom;
    }

    private void setNom(String nom) {
        this.nom = (nom==null?"Produit générique":nom.toUpperCase());
    }

    //toString
    @Override
    public String toString() {
        return this.getClass().getSimpleName() + "{" + "nom =" + "}";
    }

    //equals & hashcode
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Produit produit = (Produit) o;
        return nom.equals(produit.nom);
    }

    @Override
    public int hashCode() {
        return Objects.hash(nom);
    }
}