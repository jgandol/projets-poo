package modele;

import java.util.Objects;

public class Fabrication {

    //ATTRIBUTS D'INSTANCE
    protected int quantite;
    protected String atelier;

    //ASSOCIATIONS
    protected Produit origine;
    protected Produit destination;

    ///////////////////////////////////// C O N S T R U C T E U R S ////////////////////////////////////////////////////

    public Fabrication(int quantite, String atelier, Produit origine, Produit destination) {
        // origine et destination sont testés pâr addComposant
        this.setQuantite(quantite);
        this.setAtelier(atelier);
        this.origine = origine;
        this.destination = destination;
    }

    public Fabrication(){
        this(0,null,null,null);
    }

    //////////////////////////////// G E T T E R S      &      S E T T E R S ///////////////////////////////////////////

    public int getQuantite() {
        return quantite;
    }

    public void setQuantite(int quantite) {
        this.quantite = (quantite<=0?1:quantite);
    }

    public Produit getOrigine() {
        return origine;
    }

    public void setOrigine(Produit origine) {
        this.origine = origine;
    }

    public Produit getDestination() {
        return destination;
    }

    public void setDestination(Produit destination) {
        this.destination = destination;
    }

    public String getAtelier() {
        return atelier;
    }

    public void setAtelier(String atelier) {
        this.atelier = (atelier==null?"LILLE":atelier.toUpperCase());
    }

    //Equals et Hashcode
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Fabrication that = (Fabrication) o;
        return origine.equals(that.origine) &&
                destination.equals(that.destination);
    }

    @Override
    public int hashCode() {
        return Objects.hash(origine, destination);
    }

    //toString

    @Override
    public String toString() {
        return "Fabrication{" +
                "quantite=" + quantite +
                ", atelier='" + atelier + '\'' +
                ", origine=" + origine.getNom() +
                ", destination=" + destination.getNom() +
                '}';
    }
}