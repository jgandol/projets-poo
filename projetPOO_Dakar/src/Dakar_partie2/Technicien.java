package Dakar_partie2;

public class Technicien extends Personne {
    //attributs d'instance
    private String specialite;

    //Getter/setter

    public String getSpecialite() {
        return specialite;
    }

    private void setSpecialite(String specialite) {
        this.specialite = (specialite == null? "mecanicien" : specialite.toLowerCase());
    }

    //Constructeurs
    public Technicien(String nom, String prenom, String specialite) {
        super(nom, prenom);
        this.setSpecialite(specialite);
    }


    public Technicien(String nom, String prenom) {
        this(nom, prenom,null);
    }

    public Technicien() {
        this.setSpecialite("mecanicien");
    }

    //regles metier
    @Override
    protected boolean estCompatible(Voiture v) {
        if(v==null)return false;
        //return (v instanceof Camion);
        return v.getClass() == Camion.class;
    }

    //toString
    @Override
    public String toString() {
        return super.toString() +
                "/ specialite= '" + specialite + '\'';
    }

    //main
    public static void main(String[] args) {
        Personne p = new Pilote("Afeux","Pierre");
        Voiture v = new Voiture(5000,'D',"tesla");
        Voiture c = new Camion(2000,'D', "CROwn", 10);

        p.affecter(v);
        System.out.println(p);

        p.affecter(c);
        System.out.println(p);
    }

}
